Class MCHPUserLoginService.Cls.MCHPUserLoginService.MCHPUserLoginServiceUnitTest.MCHPUserLoginServiceTest Extends %UnitTest.TestCase
{

	Method TestValidateUser()
	{
		Set tSC=$$$OK
		Set tError = ##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Error).%New()
		Set tBody = ##class(%ZEN.proxyObject).%New()
		Set tResp = ##class(%ZEN.proxyObject).%New()
		Set tBody.LoginID="John"
		Set tBody.Password="test"
		Set tStatus =tBody.%ToJSON()
		If $$$ISERR(tStatus) || '$IsObject(tBody) {
			Write "Error in JSON"
	 		Quit $$$ERROR($$$GeneralError,"Error in JSON")
	 	}
	 	
		Do $$$AssertEquals(##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidateUser(tBody,tError,tResp),1,"Expected value for validation pass=1")
	} 
	
	Method TestValidateUserDemographics()
	{
		Set tSC=$$$OK
		Set tError = ##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Error).%New()
		Set tBody = ##class(%ZEN.proxyObject).%New()
		Set tResp = ##class(%ZEN.proxyObject).%New()
		Set tBody.lastName="king"
		Set tBody.firstName="John"
		Set tStatus =tBody.%ToJSON()
		If $$$ISERR(tStatus) || '$IsObject(tBody) {
			Write "Error in JSON"
	 		Quit $$$ERROR($$$GeneralError,"Error in JSON")
	 	}
	 	
		Do $$$AssertEquals(##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidateDemographics(tBody,tError),1,"Valid demographics expected=1")
	} 
	
	Method TestValidateUserEmail()
	{
		Set tEmail="jking@yahoo.com"
		Do $$$AssertEquals(##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidateEmail(tEmail),1,"Valid EmailID expected=1")
	}
	
	Method TestValidateUserPhone()
	{
		Set tPhone="212-568-8963"
		Do $$$AssertEquals(##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidatePhone(tPhone),1,"Valid Phone expected=1")
	}
	
	/// Runs the test methods in this unit test class.
ClassMethod Run(ByRef pUTManager As %UnitTest.Manager = "", pBreakOnError As %Boolean = 0) 
{
	Try
	{
	 Set tSuccess = 1
	 //Write "*** Unit tests starting at  ",$zdt($h,3)," ***",!
	// Write "$zh : "_$zh,!
     Set tBegin = $zh
    If '$IsObject(pUTManager) {
        Set pUTManager = ##class(%UnitTest.Manager).%New() //Or Tools.UnitTest.Manager if you have that
        Set pUTManager.Debug = pBreakOnError
        Set pUTManager.Display = "log,error"
    }
    Set tTestSuite = $Piece($classname(),".",1,*-1)
    //Write "tTestSuite : "_tTestSuite,!
    Set qspec = "/noload/nodelete"
    Set tSC = $$$qualifierParseAlterDefault("UnitTest","/keepsource",.qspec,.qstruct)
    //Write "$Replace : "_$Replace(tTestSuite,".","/") ,!
    //Write "_tTestSuite : "_tTestSuite_":"_$classname() ,!
    Do pUTManager.RunOneTestSuite("",$Replace(tTestSuite,".","/"),tTestSuite_":"_$classname(),.qstruct)
    
    If $IsObject(pUTManager) {
    //Write "$zh : "_$zh,!
    //Write "$zh-tBegin : "_$zh-tBegin,!
            Do pUTManager.SaveResult($zh-tBegin)
            Do pUTManager.PrintURL()
    
            &sql(select sum(case when c.Status = 0 then 1 else 0 end) as failed,
                        sum(case when c.Status = 1 then 1 else 0 end) as passed,
                        sum(case when c.Status = 2 then 1 else 0 end) as skipped
                        into :tFailed, :tPassed, :tSkipped
                   from %UnitTest_Result.TestSuite s
                   join %UnitTest_Result.TestCase c
                     on s.Id = c.TestSuite
                  where s.TestInstance = :pUTManager.LogIndex)

            If (tFailed '= 0) {
                Set tSuccess = 0
            }
        } Else {
            Write "No unit tests found in suite",!
        }
    } Catch anyException {
        Set tSuccess = 0
        Write anyException.DisplayString(),!
    }
    //Write !,!,"Test cases: ",tPassed," passed, ",tSkipped," skipped, ",tFailed," failed",!
    If 'tSuccess {
        Write !,"ERROR(S) OCCURRED."
    }
    Quit $Select(tSuccess:1,1:$$$ERROR($$$GeneralError,"One or more errors occurred in unit tests."))
    //Quit tSC
}
	
}