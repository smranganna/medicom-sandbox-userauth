Class MCHPUserLoginService.Cls.MCHPUserLoginService.JSON.User Extends (%RegisteredObject,%XML.Adaptor)
{
	Property LoginID As %String;
	Property demographics As JSON.Demographics;
	Property Type As %String;
	Property Suffix As %String;
	Property Description As %String;
	Property MessageRepository As %String;
	Property DomainCode As %String;
	Property Location As %String;
	Property Specialty As %String;
	Property SubSpecialty As %String;
	Property Password As HS.Registry.Person.UserPassword(MAXLEN = 20);
	Property PasswordExternal As %String(MAXLEN = 128)[ InitialExpression = {$c(0)}, Transient ];
	/// Change password on next login.<br>
	/// 0 - Password change not required.<br>
	/// 1 - Password change required before next login.<br>
	Property ChangePassword As %Boolean [ InitialExpression = 0 ];
	Property DatePasswordChanged As %Integer;
	Property ExpirationDate As %Integer;
	Property Roles As list Of %String(MAXLEN = 64);
	/// Required for use of "Forgot Password"
	Property ChallengeQuestion As %String(MAXLEN = 256);
	Property ChallengeAnswer As %String(MAXLEN = 256);
	Property Telecom As JSON.Telecom;
	Property Identifier As JSON.Identifier;
	Property Facility As JSON.Facility;
	/// If the domain uses two factor authentication, this will be the phone provider
	Property PhoneProvider As %String;
	/// If the domain uses two factor authentication, this will be the phone number (usually a mobile phone)
	Property PhoneNumber As %String;
}