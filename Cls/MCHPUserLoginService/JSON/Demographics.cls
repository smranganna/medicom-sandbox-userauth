Class MCHPUserLoginService.Cls.MCHPUserLoginService.JSON.Demographics Extends (%RegisteredObject,%XML.Adaptor)
{
Property enteredLat As %String;

Property enteredLong As %String;

Property enteredAddress As %String;

Property enteredBy As %String;

Property enteredOn As %String;

Property Address As JSON.Address;

Property dob As %String;

Property email As %String;

Property firstName As %String;

Property sex As %String;

Property homePhone As %String;

Property lastName As %String;

Property middleName As %String;

Property mobilePhone As %String;

Property photoURL As %String;

Property workPhone As %String;
}