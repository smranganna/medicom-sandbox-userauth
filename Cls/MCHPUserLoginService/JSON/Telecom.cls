Class MCHPUserLoginService.Cls.MCHPUserLoginService.JSON.Telecom Extends (%RegisteredObject,%XML.Adaptor)
{
	Property PhoneNumberFull As %String;

	Property PhoneCountryCode As %String;

	Property PhoneArea As %String;
	
	Property PhoneAreaCode As %String;
	
	Property PhoneNumber As %String;
	
	Property MobileArea As %String;
	
	Property MobileNumber As %String;
	
	Property FaxArea As %String;
	
	Property FaxNumber As %String;
	
	Property PagerArea As %String;
	
	Property PagerNumber As %String;
	
	Property URL As %String;
	
	Property Email As %String(MAXLEN = 500);
	
	/// Used to specify what kind of telecom this is:  Fax, Pager, Land, or Mobile
	Property Type As %String(DISPLAYLIST = ",Fax,Pager,Land,Mobile,Other", VALUELIST = ",F,P,L,M,O");
	
	/// Used to specify how this telecom will be used:  Primary Home, Vacation Home, Work Place, etc...
	/// Values taken from HL7v3 TelecommunicationAddressUse Domain (OID: 2.16.840.1.113883.1.11.201)
	/// Also see:	http://www.hl7.org/documentcenter/public_temp_77F950A7-1C23-BA17-0C5765ECF9A9F9C5/standards/vocabulary/vocabulary_tables/infrastructure/vocabulary/vs_AddressUse.html#TelecommunicationAddressUse
	Property Use As %String(DISPLAYLIST = ",Answering Service,Bad,Direct,Emergency Contact,Home,Primary Home,Vacation Home,Mobile Contact,Pager,Public,Temporary,Work Place,", VALUELIST = ",AS,BAD,DIR,EC,H,HP,HV,MC,PG,PUB,TMP,WP,");
	
	Property EmailType As %String;
	
	Property PrimaryFlag As %Boolean;
	
	Property Status As %String(DISPLAYLIST = ",Active,Inactive", VALUELIST = ",A,I");
}