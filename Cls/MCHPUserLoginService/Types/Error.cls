Class MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Error Extends (%RegisteredObject,%XML.Adaptor)
{
	Parameter BADINPUT = 1;

	Parameter UNKNOWNAUTHTYPE = 2;
	
	Parameter INVALIDLOGIN = 3;
	
	Parameter INTERNALERROR = 4;
	
	Property errorCode As %Integer;
	
	Property errorString As %String;
}