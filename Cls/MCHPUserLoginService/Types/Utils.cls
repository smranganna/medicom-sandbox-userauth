Class MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils Extends %RegisteredObject
{
/// Validate User
ClassMethod ValidateUser(pReq As JSON.User, ByRef pError As Types.Error, ByRef pProxy) As %Status
{
	Set tStatus = $$$OK
	Write "ValidateUser",!
	//Validate user login Info	
	If pReq.LoginID=""
	{
		Set pError.errorCode = ##class(Types.Error).#BADINPUT
		Set pError.errorString = "LoginID cannot be empty"
		Set pProxy.error = pError
		Quit $$$ERROR($$$GeneralError,"LoginID cannot be empty")
	}
	Write "LoginID :"_pReq.LoginID,!
	//password
 	If pReq.Password = "" 
 	{
 		Set pError.errorCode = ##class(Types.Error).#BADINPUT
		Set pError.errorString = "password cannot be empty"
		Set pProxy.error = pError
		Quit $$$ERROR($$$GeneralError,"password cannot be empty")
 	}
	Quit tStatus
}

ClassMethod ValidateDemographics(pReq As JSON.Demographics, ByRef pError As Types.Error) As %Status
{
	Write "ValidateDemographics",!
	Set tSC = $$$OK
	Try {
		if pReq.lastName="" {
			Set pError.errorCode = ##class(Types.Error).#BADINPUT
			Set pError.errorString = "Last name cannot be empty"
			Set tSC = $$$ERROR($$$GeneralError,"Last name cannot be empty")
		}
		Write "lastname :"_pReq.lastName,!
		if pReq.firstName="" {
			Set pError.errorCode = ##class(Types.Error).#BADINPUT
			Set pError.errorString = "First name cannot be empty"
			Set tSC=$$$ERROR($$$GeneralError,"First name cannot be empty")
		}
		Write "firstname :"_pReq.firstName,!
	}
	
	Catch eEx {
		Set tSC = eEx.AsStatus()
	}
	Quit tSC
}


ClassMethod ValidateEmail(pEmail As %String) As %String
{
	write "validate email routine:"_pEmail,!
  //#dim validEmailPattern="^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$" 
            
  //set matcher=##class(%Regex.Matcher).%New(validEmailPattern,pEmail)
    IF $MATCH(pEmail,"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$")
  {write "email valid",!
   quit 1}
  else
  {write "email invalid",!
  quit 0}

  Quit 1
}

ClassMethod ValidatePhone(pPhone As %String) As %String
{
	write "validate phone routine: "_pPhone,!
            
    IF $MATCH(pPhone,"(\((\d{3})\)\s*|\b(\d{3})-)(\d{3})-(\d{4})\b")
  {write "phone valid",!
   quit 1}
  else
  {write "phone invalid",!
  quit 0}

  Quit 1
	}
}