Include (Ensemble)

/// User login REST service 
Class MCHPUserLoginService.Cls.MCHPUserLoginService.Service.REST.Login Extends (%CSP.REST)
{

XData UrlMap
{
<Routes>
<Route Url="/SignUp" Method="Post" Call="SaveUser"/>
<Route Url="/Login/:UserID" Method="Get" Call="LoginUser"/>
</Routes>
}

/// Save/Add User
ClassMethod SaveUser() As %Status
{
	Write "SaveUser",!
	Set tSC = $$$OK
	Try {
		Set tError = ##class(Types.Error).%New()
		Set tReq = ##class(JSON.User).%New()
		Set tReqDemographics = ##class(JSON.User).%New()
		Set tResp = ##class(%ZEN.proxyObject).%New()
		
		// Parse the incoming JSON
		Write "request content :" _%request.Content,!
 		Set tSC = ##class(%ZEN.Auxiliary.jsonProvider).%ParseJSON(%request.Content,,.tReq)
 		Write "treq :" _tReq.LoginID,!
 		If $$$ISERR(tSC) || '$IsObject(tReq) {
 		Write "%response.Status :" _%response.Status,!
	 		Set %response.Status = ..#HTTP400BADREQUEST
	 		If $$$ISERR(tSC) {
				Set tError.errorCode = ##class(Types.Error).#BADINPUT
				Set tError.errorString = $system.Status.GetErrorText(tSC)
				Set tResp.error = tError
	 		}
 		Quit 
 		}
 		
 		//ValidateUser
 		Write "Check ValidateUser",!
 		Set tSC= ##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidateUser(tReq,tError,.tResp)
		If (tSC=..#HTTP400BADREQUEST){Quit}

		Set tReqDemographics=tReq.demographics
		Set tSC= ##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidateDemographics(tReqDemographics,tError,.tResp)
		If (tSC=..#HTTP400BADREQUEST){Quit}
		
		//SaveUserData
		Set tSC=..SaveUserData(tReq,tError,.tResp)
		If (tSC=..#HTTP400BADREQUEST){Quit}
		}
		Catch eEx {
		Set tError.errorCode = ##class(Types.Error).#INTERNALERROR
		Set tError.errorString = "Internal Error registering user.  Please contact CHP MDDS system administrator.Error:"
		Set tResp.error = tError
		Set %response.Status = ..#HTTP500INTERNALSERVERERROR
		$$$LOGERROR("Error creating User:"_eEx.DisplayString())
	}
	Quit tSC
}
	
ClassMethod SaveUserData(pReq As JSON.User, ByRef pError As Types.Error, ByRef pProxy) As %Status
{
	Write "SaveUserData",!
	Set tSC = $$$OK
	Set tComposite=##class(HS.Registry.Person.User.WebServices.Containers.Composite).%New()
	
	// User fields
	Set tUser = ##class(HS.Registry.Person.User.WebServices.Containers.User).%New()
	Set tUser.Active=1
	Set tUser.UserID=pReq.LoginID
	Set tUser.Suffix = pReq.Suffix
	Set tUser.Description =  pReq.Description
	Set tUser.MessageRepository =  pReq.MsgRespository
	If ($IsObject(pReq.demographics))
	{
		write "demographics current address",!
		Set tUser.LastName = pReq.demographics.lastName
		Set tUser.FirstName = pReq.demographics.firstName
		Set tUser.MiddleName = pReq.demographics.middleName
		write "email: "_pReq.demographics.email,!
		Set ok=##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidateEmail(pReq.demographics.email)
		If (ok'=1) 
		{write "invalid email",! Quit $$$ERROR("Invalid email")}
		else
		{Set tUser.EmailAddress = pReq.demographics.email}
		If ($IsObject(pReq.demographics.Address))
		{
		write "current address",!
		Set tUser.Street = pReq.demographics.Address.street
		Set tUser.City = pReq.demographics.Address.city
		Set tUser.State = pReq.demographics.Address.state
		Set tUser.Zip = pReq.demographics.Address.zip
		}
		Set tUser.Type=  pReq.Type
		Set tUser.Location = pReq.Location
		Set tUser.Specialty = pReq.Specialty
		Set tUser.SubSpecialty = pReq.SubSpecialty
		Write "$username"_$username,!
		Set tUser.%User=$username
		Write "$roles"_$roles,!
		Set tUser.%Roles=$roles
	}
	
	// Telecom data
	Set tTelecom=##class(HS.Types.Telecom).%New()
	If ($IsObject(pReq.Telecom))
	{
		Write "Telecom",!
		Set tTelecom.PhoneAreaCode=pReq.Telecom.PhoneArea
		Set ok=##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidatePhone(pReq.Telecom.PhoneNumber)
		If (ok'=1) 
		{write "invalid phone",! Quit $$$ERROR("Invalid phone number")}
		else
		{Set tTelecom.PhoneNumber=pReq.Telecom.PhoneNumber}
		Set tTelecom.Type="L"
		Set tUser.Phone=tTelecom
		Set tTelecom=##class(HS.Types.Telecom).%New()
		Set tTelecom.PhoneAreaCode=pReq.Telecom.MobileArea
		Set ok=##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidatePhone(pReq.Telecom.MobileNumber)
		If (ok'=1) 
		{write "invalid phone",! Quit $$$ERROR("Invalid phone number")}
		else
		{Set tTelecom.PhoneNumber=pReq.Telecom.MobileNumber}
		Set tTelecom.Type="M"
		Set tUser.Mobile=tTelecom
		Set tTelecom=##class(HS.Types.Telecom).%New()
		Set tTelecom.PhoneAreaCode=$S($E(pReq.Telecom.FaxArea,*)="-":$E(pReq.Telecom.FaxArea,1,*-1),1:pReq.Telecom.FaxArea)
		Write "tTelecom.PhoneAreaCode"_tTelecom.PhoneAreaCode,!
		Set ok=##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidatePhone(pReq.Telecom.FaxNumber)
		If (ok'=1) 
		{write "invalid phone",! Quit $$$ERROR("Invalid phone number")}
		else
		{Set tTelecom.PhoneNumber=pReq.Telecom.FaxNumber}
		Set tTelecom.Type="F"
		Set tUser.Fax=tTelecom
		Set tTelecom=##class(HS.Types.Telecom).%New()
		Set tTelecom.PhoneAreaCode=$S($E(pReq.Telecom.PagerArea,*)="-":$E(pReq.Telecom.PagerArea,1,*-1),1:pReq.Telecom.PagerArea)
		Set tTelecom.PhoneNumber=pReq.Telecom.PagerNumber
		Set tTelecom.Type="P"
		Set tUser.Pager=tTelecom
	}
	
	// Clinician identifier
	If ($IsObject(pReq.Identifier))
	{
		Write "Identifier",!
		If (pReq.Identifier '= "") {
		Set tIdentifier=##class(HS.Types.Identifier).%New()
		Set tIdentifier.Extension=pReq.Identifier
		Set tIdentifier.AssigningAuthorityName=pReq.Identifier.AssigningAuthorityName
		Set tIdentifier.Use="DN"
		Set tIdentifier.Status=pReq.Identifier.Status
		Do tUser.Identifiers.Insert(tIdentifier)
		}
	}	
		
	// Facility
	If ($IsObject(pReq.Facility))
	{
		Write "Facility",!
		If (pReq.Facility.FacilityId]"") {
		Set tFacility = ##class(HS.Registry.Person.User.WebServices.Containers.UserFacility).%New()
		Set tFacility.Facility = pReq.Facility.FacilityId
		Set tFacility.PrimaryFlag=1
		Set tComposite.FacilityContainer=tFacility
		}
	}
	
	// Login fields
	Set tLogin = ##class(HS.Registry.Person.User.WebServices.Containers.UserLoginID).%New()
	Set tLogin.LoginID = pReq.LoginID
	Write "loginID"_pReq.LoginID,!
	Set tLogin.Domain = pReq.DomainCode
	Write "DomainCode"_pReq.DomainCode,!
	Set tLogin.ChangePassword = pReq.ChangePassword
	Write "changepwd"_pReq.ChangePassword,!
	Set tLogin.ChallengeQuestion = pReq.ChallengeQuestion
	Write "pReq.ChallengeQuestion"_pReq.ChallengeQuestion,!
	Set tLogin.ChallengeAnswer = pReq.ChallengeAnswer
	Write "pReq.ChallengeAnswer"_pReq.ChallengeAnswer,!
	Set ok=##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidatePhone(pReq.PhoneNumber)
	If (ok'=1) 
	{write "invalid phone",! Quit $$$ERROR("Invalid phone number")}
	else
	{Set tLogin.PhoneNumber=pReq.PhoneNumber}
	Write "pReq.PhoneNumber"_pReq.PhoneNumber,!
	Set tLogin.PhoneProvider=pReq.PhoneProvider
	Write "pReq.PhoneProvider"_pReq.PhoneProvider,!
	Set tLogin.Roles=pReq.Roles
	Write "roles"_tLogin.Roles,!
 //	For tI=1:1:$L(pReq.Groups,",") {
 //		Set tRole=$P(pReq.Groups,",",tI)
 //		Do:tRole'="" tLogin.Roles.Insert(tRole)
 //	}
 //	
	// Converts external to internal password
	If pReq.Password]"" {
		Set tObj=##class(HS.Registry.Person.UserLoginID).%New()
		Set tObj.PasswordExternal=pReq.Password
		Set tLogin.Password=tObj.Password
		Write "tLogin.Password"_tLogin.Password,!
		Set tLogin.Salt=tObj.Salt
		Write "tLogin.Salt"_tLogin.Salt,!
	}
	
	// Check to stop save process if the userID is duplicated on any other user, active or inactive.
	Set tSC=##class(HS.Util.HubServices).InvokeHubService("HS.Registry.Person.User.WebServices","HS.Registry.Person.User.WebServicesClient","CheckDuplicateUser",.tObj,tUser)
	Set ok=..%CheckError(tSC,$G(tObj))
	If (ok'=1) {
		write "error with service check duplicates "_tSC,!
		// If we have an error message, quit and return it to be displayed to the user
		Quit tSC
	}
	else
	{write "success with service check dupliccate "_tSC,!}
	// Save user
	Set tObj=##Class(HS.Registry.Person.User).%New()
	Set tObj.UserID=pReq.LoginID
	Set tObj.Description=pReq.Description
	Set tObj.Name=pReq.Description
	Set tObj.Active=1
	Set tObj.CacheUser=0
	If ($IsObject(pReq.demographics))
	{
		Set tObj.NameParts.Given=pReq.demographics.firstName
		Set tObj.NameParts.Family=pReq.demographics.lastName
		Set tObj.NameParts.Middle=pReq.demographics.middleName
		Set ok=##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidateEmail(pReq.demographics.email)
		If (ok'=1) {write "invalid email, save user",! Quit $$$ERROR("Invalid email")}
		else
		{Set tUser.EmailAddress = pReq.demographics.email}
		If ($IsObject(pReq.demographics.Address))
		{
		write "current address",!
		Set tObj.BirthPlace.StreetName = pReq.demographics.Address.street
		Write "tObj.BirthPlace.StreetName"_tObj.BirthPlace.StreetName,!
		Set tObj.BirthPlace.City = pReq.demographics.Address.city
		Set tObj.BirthPlace.State = pReq.demographics.Address.state
		//Set tObj.BirthPlace.Zip = pReq.demographics.Address.zip
		}
		Set tObj.Type=  pReq.Type
		Set tObj.Location = pReq.Location
		Set tObj.Specialty = pReq.Specialty
		Set tObj.SubSpecialty = pReq.SubSpecialty
		Write "$roles from demogrPHIC SOBJECT"_$roles,!
		Set tUser.%Roles=$roles
		}
	Set tLogin=##Class(HS.Registry.Person.UserLoginID).%New()
	Set tLogin.DomainCode=pReq.DomainCode
	Set tLogin.LoginID=tObj.UserID
	Set tLogin.PasswordExternal=pReq.Password
	//Set tLogin.Password=tObj.Password
	Set tLogin.DatePasswordChanged=$P($H,",")
	Set tLogin.Roles=pReq.Roles
	Set tLogin.ChangePassword = pReq.ChangePassword
	Set tLogin.ChallengeQuestion = pReq.ChallengeQuestion
	Set tLogin.ChallengeAnswer = pReq.ChallengeAnswer
	Set ok=##class(MCHPUserLoginService.Cls.MCHPUserLoginService.Types.Utils).ValidatePhone(pReq.PhoneNumber)
	If (ok'=1) 
	{write "invalid phone",! Quit $$$ERROR("Invalid phone number")}
	else
	{Set tLogin.PhoneNumber=pReq.PhoneNumber}
	Set tLogin.PhoneProvider=pReq.PhoneProvider
	Set tLogin.UserClinician=tObj
	Set tSC=tObj.%Save()
	Set ok=..%CheckError(tSC,$G(tObj))
	// Handle the rare case where the user gets saved, but the provider errors out (as they must be in separate transactions)
	If (ok'=1) {
	 	Write "error saving userClinician data "_tSC,!
		Do $system.Status.DecomposeStatus($S($IsObject($G(tObj)):tObj.Status,1:tSC),.tErr)
		If $G(tErr(1,"code"))=$$$UnkAndUnexpectedError {
			Write "Unexpected error while saving userClinician data ",!
			Set ok=1
		}
		Quit tSC
	}
	Set tComposite.UserContainer=tUser
	Set tComposite.LoginContainer=tLogin
	Set tSC=##class(HS.Util.HubServices).InvokeHubService("HS.Registry.Person.User.WebServices","HS.Registry.Person.User.WebServicesClient","AddEditUserComposite",.tObj,tComposite)
	// Handle the rare case where the user gets saved, but the provider errors out (as they must be in separate transactions)
	Set ok=..%CheckError(tSC,$G(tObj))
	// Handle the rare case where the user gets saved, but the provider errors out (as they must be in separate transactions)
	If (ok'=1) {
	 	Write "error with service check addedituser "_tSC,!
		Do $system.Status.DecomposeStatus($S($IsObject($G(tObj)):tObj.Status,1:tSC),.tErr)
		If $G(tErr(1,"code"))=$$$UnkAndUnexpectedError {
			Write "Unexpected error ",!
			Set ok=1
		}
	}
	else
	{write "success with service check addedituser"_tSC,!}
	Quit tSC
}

/// Check for an error from a web service call to load an object
ClassMethod %CheckError(pSC As %Status, pObj As HS.Util.WebServices.ServiceStatus) As %String
{
	If '$$$ISOK(pSC) {
		Set ok=$System.Status.GetErrorText(pSC)
	} ElseIf '$IsObject(pObj) {
		Set ok="Cannot open object!"
	} ElseIf '$$$ISOK(pObj.Status) {
		Set ok=$System.Status.GetErrorText(pObj.Status)
	} Else {
		Quit 1
	}
	Write "ok: "_ok,!
	Quit ..%FormatError(ok)
}

/// Make error message presentable
ClassMethod %FormatError(pMsg As %String) As %String
{
	Write "Msg: "_pMsg,!
	Quit $S($F(pMsg,":"):$P($P(pMsg,":",2,99),"[",1),1:pMsg)
}

	
/// Login User
ClassMethod LoginUser(pUserID As %String) As %Status
{
	Write "LoginUser "_pUserID,!
	Set tSC = $$$OK
	Try {
		Set tError = ##class(Types.Error).%New()
		Set tResp = ##class(%ZEN.proxyObject).%New()
		
		//LoginUser
		Set tSC=..LoginUserData(pUserID,.tResp)
		If (tSC=..#HTTP400BADREQUEST){Quit}
		do tResp.%ToJSON()
		}
		Catch eEx {
		Set tError.errorCode = ##class(Types.Error).#INTERNALERROR
		Set tError.errorString = "Internal Error signing user  Please contact CHP MDDS system administrator.Error:"
		//Set tResp.error = tError
		Set %response.Status = ..#HTTP500INTERNALSERVERERROR
		$$$LOGERROR("Error creating User:"_eEx.DisplayString())
	}
	Quit tSC
}

ClassMethod LoginUserData(pUserID As %String,ByRef pResp) As %Status
{
	Write "LoginUserData "_pUserID,!
	set tStatement = ##class(%SQL.Statement).%New()
	Set tLogin=##Class(HS.Registry.Person.UserLoginID).%New()
	Set tObj=##Class(HS.Registry.Person.User).%New()
	try {
		do tStatement.prepare("SELECT UserClinician, DomainCode,LoginID,PhoneNumber, PhoneProvider, Roles FROM HS_Registry_Person.UserLoginID Where LoginID= ?" )
		set tResult = tStatement.%Execute(pUserID)
			Write "tResult  "_tResult ,!
			While tResult.%Next() {
			Set pResp.DomainCode=tResult.%Get("DomainCode")
			Set pResp.LoginID=tResult.%Get("LoginID")
			Set pResp.PhoneNumber=tResult.%Get("PhoneNumber")
			Set pResp.PhoneProvider=tResult.%Get("PhoneProvider")
			}
		set tReturn = 1
	}
	catch tException {
		#dim tException as %Exception.AbstractException
		//set %sqlcontext.%SQLCODE = tException.AsSQLCODE(), %sqlcontext.%Message = tException.SQLMessageString()
		set tReturn = 0
	}
	quit tReturn

	}

ClassMethod FindUsersExecute(ByRef qHandle As %Binary, pActiveOnly As %Boolean = 1, pLastName As %String, pFirstName As %String, pUserId As %String, pNumber As %String, pAssignAuth As %String, pCliniciansOnly As %Boolean, pFilterFlag As %Boolean, pFacilities As %String, pRelationshipId As %String, pMPIID As %String, pDelivery As %Boolean) As %Status [ ServerOnly = 1 ]
{
	Set tSC=##class(HS.Util.HubServices).InvokeHubService("HS.Registry.Person.User.WebServices","HS.Registry.Person.User.WebServicesClient","FindUsers",.tObj,pActiveOnly,pLastName,pFirstName,pUserId,pNumber,pAssignAuth,pCliniciansOnly,pFilterFlag,pFacilities,pRelationshipId,pMPIID,pDelivery)
	If '$$$ISOK(tSC) Quit tSC
	If '$$$ISOK(tObj.Status) Quit tObj.Status
	Set qHandle=tObj
	Set qHandle("POS")=0
	Quit $$$OK
}
}